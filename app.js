let balance = 0;
let salary = 0;
let loanAmount = 0;
let hasLoan = false;

//This lets you put "kr" after the number displayed
const convertToCurrency = (number) => Intl.NumberFormat("en-SE",{style: "currency", currency: "SEK", minimumFractionDigits: 0}).format(number);
const payPerClick = 100;

//Adds pay to your salary
function getPaid() {
    salary += payPerClick;
    updateBalances()
}
      
function transferToBank() {
    // If user has an active loan 10% of the salary will go to paying it back
    if (hasLoan) {
        let deduction = salary * 0.1;
        if(deduction > loanAmount) {
            deduction = loanAmount;
        }
        salary -= deduction;
        loanAmount -= deduction;
        balance += salary;
    } else {
        balance += salary;   
    }
    salary = 0;
    loanPaidBackCheck();
    updateBalances()
}     

function loan() {
    /* To be allowed a loan, user cannot have an already existing loan and can only loan up
    to twice the amount that is in his/her balance*/
    if (hasLoan) {
        alert("You already have a loan. Pay it back first. Greetings from Mr. Joe Banker");
        return;
    }
    tempAmount = prompt("Amount you want to loan")
    if (!hasLoan && tempAmount <= balance * 2) {
    tempAmount = parseFloat(tempAmount);
    if(isNaN(tempAmount) || tempAmount <= 0) return;
    
    loanAmount = tempAmount;
    balance += loanAmount;
    hasLoan = true;      

    loanPaidBackCheck();
    updateBalances()
    }
    else {
        //Deny the loan. User already has an active loan.
        alert("Loan denied. You either already have a loan or do not have the financial means to take on this big of a loan.🤣🤣");
    }
}

function repayLoan() {
    // If user pays more than the amount loaned the rest will be transferred to his/her balance
    if (loanAmount <= salary) {
        salary -= loanAmount;
        balance += salary;
        loanAmount = 0;
        salary = 0;
    // If user pays less than the loaned amount the full salary will go towards paying back the loan
    } else {
        loanAmount -= salary;
        salary = 0;
    }
    updateBalances()
    loanPaidBackCheck();
}

function buy() {
    let selectedComputer = computers[computersElement.selectedIndex]
    let price = selectedComputer.price;
    if (balance >= price) {
        balance -= price;
        alert("You bought a new laptop! It will be shipped shortly.");
    } else {
        alert("You cannot afford this computer.");
    }
    updateBalances()
}

//Checks if the loan is paid back in full, if true it hides the loan container, if false it makes the loan container visible
function loanPaidBackCheck() {
    //If loan is zero, less or null it is deemed as paid back.
    if (loanAmount <= 0 || loanAmount == null || loanAmount == NaN) {
        hasLoan = false;
        document.getElementById("loancontainer").style.visibility = "hidden";
        document.getElementById("pay-loan-button").style.visibility = "hidden";
    } else {
        document.getElementById("loancontainer").style.visibility = "visible";
        document.getElementById("pay-loan-button").style.visibility = "visible";
    }
}

//updates to html text
//#################################################
function updateBalances() {
    document.getElementById("balance-text").innerHTML = convertToCurrency(balance);
    document.getElementById("salary-text").innerHTML = convertToCurrency(salary);
    document.getElementById("loan-text").innerHTML = convertToCurrency(loanAmount);
}
//#################################################


//Connects to the HTML element by ID
const computersElement = document.getElementById("computers");
const titleElement = document.getElementById("computer-title");
const descriptionElement = document.getElementById("description");
const specsElement = document.getElementById("specs");
const priceElement = document.getElementById("price");
const imageElement = document.getElementById("image");

let computers = [];

//Fecthes laptops from API
function getComputers() {
    fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
        .then(response => response.json())
        .then(data => computers = data) 
        .then(computers => addComputersToMenu(computers))
}

const addComputersToMenu = (computers) => {
    computers.forEach(x => addComputerToMenu(x));
    priceElement.innertext = "Price - " + computers[0].price;

}

const addComputerToMenu = (computer) => {
    const computerElement = document.createElement("option");
    computerElement.value = computer.id;
    computerElement.appendChild(document.createTextNode(computer.title));
    computersElement.appendChild(computerElement);
    updateComputerInfo();
}

//Every time you choose a new computer in the drowdown this will activate
function updateComputerInfo() {
    let selectedComputer = computers[computersElement.selectedIndex]
    specsElement.innerText = "";

    for (let i = 0; i < selectedComputer.specs.length; i++) {
        specsElement.innerText += selectedComputer.specs[i] + "\n";
    }
    titleElement.innerText = selectedComputer.title;
    descriptionElement.innerText = selectedComputer.description;
    priceElement.innerText = convertToCurrency(selectedComputer.price);
    imageElement.src = "https://noroff-komputer-store-api.herokuapp.com/"+selectedComputer.image;
}

getComputers();   